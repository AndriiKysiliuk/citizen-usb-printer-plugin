var exec = require('cordova/exec');


module.exports.printCheck = function (arg0,success, error){
    exec(success , error, 'CitizenUsbPrinter' , 'printCheck' , [arg0]);
}

module.exports.printWaste = function (arg0,success, error){
    exec(success , error, 'CitizenUsbPrinter' , 'printWaste' , [arg0]);
}

module.exports.printBarcode = function (arg0,success, error){
    exec(success , error, 'CitizenUsbPrinter' , 'printBarcode' , [arg0]);
}

module.exports.printQRcode = function (arg0,success, error){
    exec(success , error, 'CitizenUsbPrinter' , 'printQRcode' , [arg0]);
}
