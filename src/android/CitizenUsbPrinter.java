package cordova.plugin.citizenusbprinter;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import com.citizen.sdk.ESCPOSConst;
import com.citizen.sdk.ESCPOSPrinter;

import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import static android.content.Context.BIND_AUTO_CREATE;

/**
 * This class echoes a string called from JavaScript.
 */
public class CitizenUsbPrinter extends CordovaPlugin {
  public Activity activity;
  public Context context;


  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    activity = cordova.getActivity();
    context = activity.getApplicationContext();


    getPermissions(permissions);

    Log.e("Citizen printer", "Initialize cordova-plugin-citizen-usb-printer");
  }


  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    switch (action) {
      case "printCheck":
        this.printCheck(args, callbackContext);
        return true;
      case "printWaste":
        this.printWaste(args, callbackContext);
        return true;
      default:
        return false;
    }
  }

  private void printCheck(JSONArray args, CallbackContext callback) {
    ESCPOSPrinter posPtr = new ESCPOSPrinter();

    // Set context
    posPtr.setContext(this.context);

    // Get Address
    UsbDevice usbDevice = null;

    PrintCheck printCheck = this.parseJsonCheck(args);

    // Connect
    int result = posPtr.connect(ESCPOSConst.CMP_PORT_USB, usbDevice);
    if (ESCPOSConst.CMP_SUCCESS == result) {
      // Character set
      posPtr.setEncoding("windows-1251");

      // Start Transaction ( Batch )
      posPtr.transactionPrint(ESCPOSConst.CMP_TP_TRANSACTION);

      // Print Normal
      posPtr.printText("не фіскальний чек\n", ESCPOSConst.CMP_ALIGNMENT_CENTER,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText(printCheck.shopName, ESCPOSConst.CMP_ALIGNMENT_CENTER,
        ESCPOSConst.CMP_FNT_BOLD | ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_2WIDTH | ESCPOSConst.CMP_TXT_2HEIGHT);

      posPtr.printNormal("\n");

      if (!printCheck.fop.isEmpty()) {
        posPtr.printText("ФОП: " + printCheck.fop, ESCPOSConst.CMP_ALIGNMENT_CENTER,
          ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);
        posPtr.printNormal("\n");
      }

      if (!printCheck.ipn.isEmpty()) {
        posPtr.printText("РНОКПП: " + printCheck.ipn, ESCPOSConst.CMP_ALIGNMENT_CENTER,
          ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);
        posPtr.printNormal("\n");
      }

      posPtr.printText("Працівник: " + printCheck.employee, ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printNormal("\n");

      posPtr.printText("Дата продажу: " + printCheck.saleDate, ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printNormal("\n");

      int nameLenght = 28;
      int priceLenght = 8;
      int quantityLenght = 4;
      int sumLenght = 8;

      posPtr.printText("Назва                      К-сть    Ціна    Сума\n",
        ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printNormal("------------------------------------------------\n");

      for (int i = 0; i < printCheck.products.size(); i++) {
        PrintProduct pr = printCheck.products.get(i);
        if (pr.name.length() > nameLenght) {

          String[] arr = pr.name.split(" ");
          StringBuilder name = new StringBuilder();

          StringBuilder tempWord = new StringBuilder();
          for (int j = 0; j < arr.length; j++) {
            if (tempWord.length() + arr[j].length() > nameLenght) {
              tempWord.append("\n");
              name.append(tempWord);
              tempWord = new StringBuilder(" " + arr[j] + " ");

              if (j == arr.length - 1) {
                name.append(arr[j]);
              }
            } else {
              if (j == arr.length - 1) {
                tempWord.append(arr[j]);
                name.append(tempWord);
              } else {
                tempWord.append(arr[j]).append(" ");
              }
            }
          }

          String[] nameArr = name.toString().split("\n");
          String lastWord = nameArr[nameArr.length - 1];
          String formatedWord = formatValue(lastWord, nameLenght, true);
          String fullName = name.toString().replace(lastWord, formatedWord);

          posPtr.printText(fullName, ESCPOSConst.CMP_ALIGNMENT_LEFT,
            ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);
        } else {
          posPtr.printText(formatValue(pr.name, nameLenght, true), ESCPOSConst.CMP_ALIGNMENT_LEFT,
            ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);
        }

        posPtr.printText(formatValue(pr.quantity, quantityLenght, false),
          ESCPOSConst.CMP_ALIGNMENT_LEFT, ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

        posPtr.printText(formatValue(pr.price, priceLenght, false),
          ESCPOSConst.CMP_ALIGNMENT_LEFT,
          ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

        posPtr.printText(formatValue(pr.sum, sumLenght, false) + "\n",
          ESCPOSConst.CMP_ALIGNMENT_LEFT,
          ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      }

      posPtr.printNormal("------------------------------------------------\n");

      if (!printCheck.client.isEmpty()) {
        posPtr.printText("Клієнт: " + printCheck.client, ESCPOSConst.CMP_ALIGNMENT_LEFT,
          ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);
        posPtr.printNormal("\n");
      }

      if (!printCheck.discount.isEmpty()) {
        posPtr.printPaddingText("Знижка:", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_RIGHT);
        posPtr.printPaddingText(printCheck.discount + " грн.", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_LEFT);
        posPtr.printNormal("\n");
      }

      if (!printCheck.bonusPay.isEmpty()) {
        posPtr.printPaddingText("Оплата бонусами:", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_RIGHT);
        posPtr.printPaddingText(printCheck.bonusPay + " грн.", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_LEFT);
        posPtr.printNormal("\n");
      }

      posPtr.printPaddingText("Всього до оплати:", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_RIGHT);
      posPtr.printPaddingText(printCheck.finalSum + " грн.", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_LEFT);
      posPtr.printNormal("\n");

      posPtr.printText("Дякуємо за покупку!\n", ESCPOSConst.CMP_ALIGNMENT_CENTER,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("Смачного!\n", ESCPOSConst.CMP_ALIGNMENT_CENTER,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);


      // Partial Cut with Pre-Feed
      posPtr.cutPaper(ESCPOSConst.CMP_CUT_PARTIAL_PREFEED);

      // End Transaction ( Batch )
      result = posPtr.transactionPrint(ESCPOSConst.CMP_TP_NORMAL);

      // Disconnect
      posPtr.disconnect();

      if (ESCPOSConst.CMP_SUCCESS != result) {
        // Transaction Error
        callback.error("Transaction Error : " + result);
      }
    } else {
      // Connect Error
      callback.error("Connect or Printer Error : " + result);
    }
  }

  private void printWaste(JSONArray args, CallbackContext callback) {
    ESCPOSPrinter posPtr = new ESCPOSPrinter();

    // Set context
    posPtr.setContext(this.context);

    // Get Address
    UsbDevice usbDevice = null;

    PrintWaste waste = this.parseJsonWaste(args);

    // Connect
    int result = posPtr.connect(ESCPOSConst.CMP_PORT_USB, usbDevice);
    if (ESCPOSConst.CMP_SUCCESS == result) {
      // Character set
      posPtr.setEncoding("windows-1251");

      // Start Transaction ( Batch )
      posPtr.transactionPrint(ESCPOSConst.CMP_TP_TRANSACTION);

      // Print Normal
      posPtr.printText("Накладна #__________від " + waste.day + "." + waste.month + "." + waste.year + "p.\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("Відпущено: ФОП " + waste.fop + " РНОКПП: " + waste.ipn + "\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("Одержано: " + waste.provider + "\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("Через: " + waste.employee + "\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("Довіреність: сер.__N____від<<___>><<___>>" + waste.year + "p.\n\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("#          Назва       Од. К-сть   Ціна   Сума  \n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("                                 без ПДВ без ПДВ\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printNormal("________________________________________________\n");


      int idLength = 4;
      int nameLenght = 19;
      int typeLength = 4;
      int quantityLenght = 5;
      int priceLenght = 8;
      int sumLenght = 8;

      for (int i = 0; i < waste.products.size(); i++) {
        PrintProduct pr = waste.products.get(i);

        posPtr.printText(formatValue(Integer.toString(i + 1), idLength, true), ESCPOSConst.CMP_ALIGNMENT_LEFT,
          ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

        if (pr.name.length() > nameLenght) {

          String[] arr = pr.name.split(" ");
          StringBuilder name = new StringBuilder();

          StringBuilder tempWord = new StringBuilder();
          for (int j = 0; j < arr.length; j++) {
            if (tempWord.length() + arr[j].length() > nameLenght) {
              tempWord.append("\n");
              name.append(tempWord);
              tempWord = new StringBuilder("    " + arr[j] + " ");

              if (j == arr.length - 1) {
                name.append("    ").append(arr[j]);
              }
            } else {
              if (j == arr.length - 1) {
                tempWord.append(arr[j]);
                name.append(tempWord);
              } else {
                tempWord.append(arr[j]).append(" ");
              }
            }
          }

          String[] nameArr = name.toString().split("\n");
          String lastWord = nameArr[nameArr.length - 1];
          String formatedWord = formatValue(lastWord, nameLenght + 4, true);
          String fullName = name.toString().replace(lastWord, formatedWord);

          posPtr.printText(fullName, ESCPOSConst.CMP_ALIGNMENT_LEFT,
            ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);
        } else {
          posPtr.printText(formatValue(pr.name, nameLenght, true), ESCPOSConst.CMP_ALIGNMENT_LEFT,
            ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);
        }

        posPtr.printText(formatValue(pr.type, typeLength, false),
          ESCPOSConst.CMP_ALIGNMENT_LEFT, ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

        posPtr.printText(formatValue(pr.quantity, quantityLenght, false),
          ESCPOSConst.CMP_ALIGNMENT_LEFT,
          ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

        posPtr.printText(formatValue(pr.price, priceLenght, false),
          ESCPOSConst.CMP_ALIGNMENT_LEFT,
          ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

        posPtr.printText(formatValue(pr.sum, sumLenght, false) + "\n",
          ESCPOSConst.CMP_ALIGNMENT_LEFT,
          ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);
      }

      posPtr.printNormal("________________________________________________\n");

      posPtr.printPaddingText("Всього без ПДВ:", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_RIGHT);
      posPtr.printPaddingText(waste.sum + " грн.", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_LEFT);
      posPtr.printNormal("\n\n");

      posPtr.printText("ПДВ(____%)\n",ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printPaddingText("Загальна сума з ПДВ:", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_RIGHT);
      posPtr.printPaddingText("__________.___грн.", ESCPOSConst.CMP_FNT_DEFAULT | ESCPOSConst.CMP_FNT_BOLD, ESCPOSConst.CMP_TXT_1WIDTH, 24, ESCPOSConst.CMP_SIDE_LEFT);
      posPtr.printNormal("\n\n");

      posPtr.printText("Всього відпущено " + waste.positionCount + " найменувань(ня)\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("на суму " + waste.money + "грн. " + waste.cent + "коп. в т.ч ПДВ=_________грн.\n\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("Директор_______________  Гол.Бух._______________\n\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);

      posPtr.printText("Відпустив______________  Одержав________________\n", ESCPOSConst.CMP_ALIGNMENT_LEFT,
        ESCPOSConst.CMP_FNT_DEFAULT, ESCPOSConst.CMP_TXT_1WIDTH | ESCPOSConst.CMP_TXT_1HEIGHT);


      // Partial Cut with Pre-Feed
      posPtr.cutPaper(ESCPOSConst.CMP_CUT_PARTIAL_PREFEED);

      // End Transaction ( Batch )
      result = posPtr.transactionPrint(ESCPOSConst.CMP_TP_NORMAL);

      // Disconnect
      posPtr.disconnect();

      if (ESCPOSConst.CMP_SUCCESS != result) {
        // Transaction Error
        callback.error("Transaction Error : " + result);
      }
    } else {
      // Connect Error
      callback.error("Connect or Printer Error : " + result);
    }
  }


  private PrintCheck parseJsonCheck(JSONArray args) {
    PrintCheck printCheck = new PrintCheck();
    try {
      JSONObject checkObj = args.getJSONObject(0).getJSONObject("check");
      printCheck.shopName = checkObj.getString("shopName");
      printCheck.fop = checkObj.getString("fop");
      printCheck.ipn = checkObj.getString("ipn");
      printCheck.employee = checkObj.getString("employee");
      printCheck.saleDate = checkObj.getString("saleDate");
      printCheck.client = checkObj.getString("client");
      printCheck.discount = checkObj.getString("discount");
      printCheck.bonusPay = checkObj.getString("bonusPay");
      printCheck.finalSum = checkObj.getString("finalSum");
      printCheck.products = new ArrayList<PrintProduct>();
      JSONArray arr = checkObj.getJSONArray("products");

      for (int i = 0; i < arr.length(); i++) {
        PrintProduct pr = new PrintProduct();
        pr.name = arr.getJSONObject(i).getString("name");
        pr.price = arr.getJSONObject(i).getString("price");
        pr.quantity = arr.getJSONObject(i).getString("quantity");
        pr.sum = arr.getJSONObject(i).getString("sum");
        printCheck.products.add(pr);
      }

    } catch (Exception e) {
      Log.e("JSON/Parse", e.getMessage());
    }

    return printCheck;
  }

  private PrintWaste parseJsonWaste(JSONArray args) {
    PrintWaste waste = new PrintWaste();
    try {
      JSONObject checkObj = args.getJSONObject(0).getJSONObject("waste");
      waste.fop = checkObj.getString("fop");
      waste.ipn = checkObj.getString("ipn");
      waste.employee = checkObj.getString("employee");
      waste.provider = checkObj.getString("provider");
      waste.day = checkObj.getString("day");
      waste.month = checkObj.getString("month");
      waste.year = checkObj.getString("year");
      waste.sum = checkObj.getString("sum");
      waste.money = checkObj.getString("money");
      waste.cent = checkObj.getString("cent");
      waste.positionCount = checkObj.getInt("positionCount");
      waste.products = new ArrayList<PrintProduct>();
      JSONArray arr = checkObj.getJSONArray("products");

      for (int i = 0; i < arr.length(); i++) {
        PrintProduct pr = new PrintProduct();
        pr.name = arr.getJSONObject(i).getString("name");
        pr.price = arr.getJSONObject(i).getString("price");
        pr.quantity = arr.getJSONObject(i).getString("quantity");
        pr.sum = arr.getJSONObject(i).getString("sum");
        pr.type = arr.getJSONObject(i).getString("type");
        waste.products.add(pr);
      }

    } catch (Exception e) {
      Log.e("JSON/Parse", e.getMessage());
    }

    return waste;
  }

  private String formatValue(String value, int fieldLength, boolean isName) {
    int valLength = value.length();
    int lengthDiff = fieldLength - valLength;
    StringBuilder valueBuilder = new StringBuilder(value);
    for (int i = 1; i <= lengthDiff; i++) {
      if (isName) {
        valueBuilder.append(" ");
      } else {
        valueBuilder.insert(0, " ");
      }
    }
    value = valueBuilder.toString();
    return value;
  }

  /**
   * Permissions
   *
   * @see <a href="https://developer.android.com/guide/topics/security/permissions#perm-groups">Dangerous permissions and permission groups.</a>
   */
  final String[] permissions = {
    android.Manifest.permission.ACCESS_FINE_LOCATION,
    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
  };
  /**
   * requestCode for {@link ActivityCompat.OnRequestPermissionsResultCallback} @Override#onRequestPermissionsResult
   */
  private static final int PERMISSION_REQUEST_CODE = 1234;


  /**
   * Get the permissions.
   *
   * @param permissions ex. {android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE,}
   */
  private void getPermissions(@NonNull final String[] permissions) {

    if (!checkPermissions(permissions)) {
      requestPermissions(permissions);
    } else {
      Log.d("getPermissions", "PERMISSION_GRANTED");
    }
  }

  /**
   * Checks whether this app has all permissions.<br>
   *
   * @param permissions ex. {android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE,}
   * @return true:All permissions granted, false:Permission denied
   */
  private boolean checkPermissions(@NonNull final String[] permissions) {

    for (String permission : permissions) {
      int permissionResult = ContextCompat.checkSelfPermission(context, permission);
////            addResultView(permission + ":" + permissionResult);
      if (permissionResult != android.content.pm.PackageManager.PERMISSION_GRANTED) {
        return false;        // non-permission
      }
    }
    return true;
  }

  /**
   * Requests permissions to be granted to this app.<br>
   *
   * @param permissions ex. {android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE,}
   */
  private void requestPermissions(@NonNull final String[] permissions) {
    ActivityCompat.requestPermissions(activity, permissions, PERMISSION_REQUEST_CODE);
  }

}

class PrintCheck {
  String shopName;
  String fop;
  String ipn;
  String employee;
  String saleDate;
  String client;
  String discount;
  String bonusPay;
  String finalSum;
  ArrayList<PrintProduct> products;
}

class PrintProduct {
  String name;
  String price;
  String quantity;
  String sum;
  String type;
}

class PrintWaste {
  String fop;
  String ipn;
  String employee;
  String provider;
  String day;
  String month;
  String year;
  String sum;
  String money;
  String cent;
  Integer positionCount;
  ArrayList<PrintProduct> products;

}
